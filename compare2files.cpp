#include <ncurses.h>
#include <string.h>
#include <fstream>
#include <string>
#include <vector>

#define N_CONFLICT 0
#define CONFLICT 1
#define FOUND 2

#define MAX_LINES 10

using namespace std;

void edit(char choice, int index, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2,vector<pair<string,int>> &outputBuff)
{   
    if (choice == 'f' && index < fileBuf1.size())
    {
        outputBuff[index].first = fileBuf1[index].first;
    }
    else if (choice == 's' && index < fileBuf2.size())
    {
        outputBuff[index].first = fileBuf2[index].first;
    }
    else if (choice == 'c')
    {
        move(25,0);
        printw("Your custom text: ");
        refresh();
        char word[60];
        getstr(word);
        outputBuff[index].first = "";
        outputBuff[index].first = word;
    }
}

 
void printBuf(int offX, int startIndex, int maxLines, vector<pair<string,int>> &fileBuf)
{
    for (int i = 0; i < min(maxLines, max((int)fileBuf.size() - startIndex,0)); i++)
    {
        move(i + 2, offX);
        if (fileBuf[i + startIndex].second == CONFLICT)
        {
            attron(A_STANDOUT);
            printw("%s", fileBuf[i + startIndex].first.c_str());
            attroff(A_STANDOUT);
        }
        else if (fileBuf[i + startIndex].second == N_CONFLICT)
        {
            printw("%s", fileBuf[i + startIndex].first.c_str());
        }
        else if (fileBuf[i + startIndex].second == FOUND)
        {
            attron(A_COLOR);
            printw("%s", fileBuf[i + startIndex].first.c_str());
            attroff(A_COLOR);
        }
        
    }
}

void compare(vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2)
{
    for (int i = 0; i < min(fileBuf1.size(), fileBuf2.size()); i++)
    {
        if (fileBuf1[i].first == fileBuf2[i].first)
        {
            fileBuf1[i].second = N_CONFLICT;
            fileBuf2[i].second = N_CONFLICT;
        }
        else
        {
            fileBuf1[i].second = CONFLICT;
            fileBuf2[i].second = CONFLICT;
        }        
    }

    if (fileBuf1.size() > fileBuf2.size())
    {
        for (int i = fileBuf2.size(); i < fileBuf1.size(); i++)
        {
            fileBuf1[i].second = CONFLICT;
        }
    }
    else if (fileBuf1.size() < fileBuf2.size())
    {
        for (int i = fileBuf1.size(); i < fileBuf2.size(); i++)
        {
            fileBuf2[i].second = CONFLICT;
        }
    }
}

int loadFile(vector<pair<string,int>> &buf, const char* fileName)
{
    string line;
    fstream file;
    file.open(fileName);
    while (getline(file, line))
    {
        buf.push_back(make_pair(line, N_CONFLICT));
    }
    file.close();
    return 0;
}

int findSubword(int index, const char* subWord, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2)
{
    int found1 = max(fileBuf1.size(),fileBuf2.size()), found2 = found1, f = 0;
    if (index < fileBuf1.size())
    {
        for (int i = index+1; i < fileBuf1.size(); i++)
        {
            if (fileBuf1[i].first.find(subWord) != -1)
            {
                found1 = i;
                i = fileBuf1.size();
                f = 1;
            }
        }
    }
    if (index < fileBuf2.size())
    {
        for (int i = index + 1; i < fileBuf2.size(); i++)
        {
            if (fileBuf2[i].first.find(subWord) != -1)
            {
                found2 = i;
                i = fileBuf2.size();
                f = 1;
            }
        }
    }
    if (f == 0)
    {
        return index;
    }
    else
    {
        return min(found1,found2);
    }
    

    
}

int conflictJump(int index, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2)
{
    int fin = max(fileBuf1.size(), fileBuf2.size());
    if (fileBuf1.size() >= fileBuf2.size())
    {
        for (int i = index + 1; i < fin; i++)
        {
            if (fileBuf1[i].second == CONFLICT)
            {
                return i;
            }
        }
    }
    else
    {
        for (int i = index + 1; i < fin; i++)
        {
            if (fileBuf2[i].second == CONFLICT)
            {
                return i;
            }
        }
    }
    
    return index;
}

void saveToFile(const char* fileName, vector<pair<string,int>> &outputBuff)
{
    ofstream file;
    file.open(fileName);
    for (int i = 0; i < outputBuff.size(); i++)
    {
        file << outputBuff[i].first << endl;
    }
    file.close();
}
int main()
{
    vector<pair<string,int>> buff1;
    vector<pair<string,int>> buff2;
    vector<pair<string,int>> outputBuff;
    

    initscr();
    char s[60];

    move(20,0);
    //printw("File1: ");
    //scanw("%s",s);

    loadFile(buff1,"file1");

    move(21, 0);
    //printw("File2: ");
    //scanw("%s", s);

    loadFile(buff2, "file2");

    outputBuff = buff1;
    for (int i = 0; i < max(buff1.size(),buff2.size())-min(buff1.size(),buff2.size()); i++)
    {
        outputBuff.push_back(make_pair("", N_CONFLICT));
    }

    compare(buff1, buff2);
    char c = 0;
    int i = 0;
    while(c != 'q')
    {   
        clear();
        if (c == 'o' && i > 0)//up
        {
            i--;
        }
        else if (c == 'l' && i < max(buff1.size(),buff2.size())-1)//down
        {
            i++;
        }
        else if (c == 'f')//search
        {   
            char find[60];
            move(22, 0);
            scanw("%s", find);
            i = findSubword(i, find, buff1, buff2);
            move(23, 0);
            printw("%d",i);
        }
        else if (c == 'd')//choose
        {
            printBuf(0, i, MAX_LINES, buff1);
            printBuf(60, i, MAX_LINES, buff2);
            printBuf(120, i, MAX_LINES, outputBuff);

            move(24,0);
            printw("first or second f/s?");
            c = getch();
            edit(c, i, buff1, buff2, outputBuff);
        }
        else if (c == 's')//save
        {
            saveToFile("OutputFile",outputBuff);
            printw("SAVED!");
            refresh();
        }
        else if (c == 'j')
        {
            i = conflictJump(i,buff1, buff2);
        }


        printBuf(0, i, MAX_LINES, buff1);
        printBuf(60, i, MAX_LINES, buff2);
        printBuf(120, i, MAX_LINES, outputBuff);
        refresh();
        c = getch();
    }
    
    
    getch();
    endwin();

    return 0;
}