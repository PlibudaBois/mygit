#include <ncurses.h>
#include <string>
#include <string.h>
#include "nowy.hpp"
#include "nano.hpp"

using namespace std;


int main(){
    NewGit mytool;
    Nano nano;
    nano.tool = &mytool;
	mytool.backend = &nano;
    mytool.init();
    nano.start();
    return 0;
}