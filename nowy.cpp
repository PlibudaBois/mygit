#include <ncurses.h>
#include <string.h>
#include <fstream>
#include <string>
#include <vector>
#include "nowy.hpp"


#define N_CONFLICT 0
#define CONFLICT 1
#define FOUND 2

using namespace std;

NewGit::NewGit()
{
    isSaved = "YES";
    index = 0;
    mode = 0;
    f1Load = 0;
    f2Load = 0;
    newText = "";   
    file1 = "";
    file2 = "";
    subword = "";
}

void NewGit::setEntry(string field, string value)
{
    if(field == "NEW_LINE")
    {
        newText = value;
    }
    else if(field == "FILE_NAME1")
    {
        file1 = value;
    }
    else if(field == "FILE_NAME2")
    {
        file2 = value;
    }
    else if(field == "SUBWORD")
    {
        subword = value;
    }
    else if (field == "SAVE_FILE")
    {
        saveFile = value;
    }
}

string NewGit::getEntry(string field)
{
    if (field == "IS_SAVED")
    {
        return isSaved;
    }
    
}

void NewGit::init()
{
    backend->bind("#nano#<LARROW>%chooseRight",[&](){chooseLeft();}, "chooseRight");
    backend->bind("#nano#<RARROW>%chooseLeft",[&](){chooseRight();}, "chooseLeft");
    backend->bind("#nano#<UARROW>%Move up",[&](){moveUp();}, "Go up");
    backend->bind("#nano#<DARROW>%Move up",[&](){moveDown();}, "Go down");
    backend->bind("#nano#<F1>%Find subword in a line!Type the subword${SUBWORD}",[&](){findSubword(subword.c_str(), buff1, buff2);}, "Find subword in a line");
    backend->bind("#nano#<F2>%Save to file!Output file name:${SAVE_FILE}",[&](){saveToFile(saveFile.c_str(),outputBuff);}, "Save to file");
    backend->bind("#nano#<F3>%Jump to another conflict line",[&](){index = conflictJump(index,buff1,buff2);redraw();}, "Jump to another conflict line");
    backend->bind("#nano#<F4>%Edit line!Type in a new line:${NEW_LINE}", [&](){edit(newText.c_str(),index,buff1,buff2,outputBuff);}, "Custom Line");
    backend->bind("#nano#<F5>%Load file!Type in first file name:${FILE_NAME1}", [&](){loadFile(buff1,file1.c_str());}, "Load first file");
    backend->bind("#nano#<F6>%Load file!Type in second file name:${FILE_NAME2}", [&](){loadFile(buff2,file2.c_str());}, "Load second file");
}//bindy

void NewGit::redraw()
{
    getmaxyx(stdscr, height, width);
    char s[60];
    
    for(int i=1; i<height-2; i++){
        move(i, 0);
        clrtoeol();
    }
    printBuf(0,index,height-3,buff1);
    printBuf(width/3,index,height-3,buff2);
    printBuf((width/3)*2,index,height-3,outputBuff);

    // move(10,10);
    // printw("%d",mode);
    //refresh();
    // getch();
    
}//wszystko

int NewGit::loadFile(vector<pair<string,int>> &buf, const char* fileName)
{
    string line;
    fstream file;
    file.open(fileName);
    while (getline(file, line))
    {
        buf.push_back(make_pair(line, N_CONFLICT));
    }
    file.close();
    if (buff1.size() != 0 && buff2.size() != 0)
    {
        compare(buff1,buff2);
        outputBuff = buff1;
        for (int i = 0; i < max(buff1.size(),buff2.size())-min(buff1.size(),buff2.size()); i++)
        {
            outputBuff.push_back(make_pair("", N_CONFLICT));
        }
    }

    redraw();
    return 0;
}

void NewGit::compare(vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2)
{
    for (int i = 0; i < min(fileBuf1.size(), fileBuf2.size()); i++)
    {
        if (fileBuf1[i].first == fileBuf2[i].first)
        {
            fileBuf1[i].second = N_CONFLICT;
            fileBuf2[i].second = N_CONFLICT;
        }
        else
        {
            fileBuf1[i].second = CONFLICT;
            fileBuf2[i].second = CONFLICT;
        }        
    }

    if (fileBuf1.size() > fileBuf2.size())
    {
        for (int i = fileBuf2.size(); i < fileBuf1.size(); i++)
        {
            fileBuf1[i].second = CONFLICT;
        }
    }
    else if (fileBuf1.size() < fileBuf2.size())
    {
        for (int i = fileBuf1.size(); i < fileBuf2.size(); i++)
        {
            fileBuf2[i].second = CONFLICT;
        }
    }
}

void NewGit::printBuf(int offX, int startIndex, int maxLines, vector<pair<string,int>> &fileBuf)
{
    for (int i = 0; i < min(maxLines, max((int)fileBuf.size() - startIndex,0)); i++)
    {
        move(i + 2, offX);
        if (fileBuf[i + startIndex].second == CONFLICT)
        {
            attron(A_STANDOUT);
            printw("%s", fileBuf[i + startIndex].first.c_str());
            attroff(A_STANDOUT);
        }
        else if (fileBuf[i + startIndex].second == N_CONFLICT)
        {
            printw("%s", fileBuf[i + startIndex].first.c_str());
        }
        else if (fileBuf[i + startIndex].second == FOUND)
        {
            attron(A_COLOR);
            printw("%s", fileBuf[i + startIndex].first.c_str());
            attroff(A_COLOR);
        }
        
    }
}
void NewGit::chooseLeft()
{
    if (index < buff1.size())
    {
        outputBuff[index].first = buff1[index].first;
    }
    redraw();
}
void NewGit::chooseRight()
{
    if (index < buff2.size())
    {
        outputBuff[index].first = buff2[index].first;
    }
    redraw();
}
void NewGit::edit(const char* choice, int index, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2,vector<pair<string,int>> &outputBuff)
{   

    outputBuff[index].first = "";
    outputBuff[index].first = choice;

    redraw();
}

int NewGit::findSubword(const char* subWord, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2)
{
    int found1 = max(fileBuf1.size(),fileBuf2.size()), found2 = found1, f = 0;
    if (index < fileBuf1.size())
    {
        for (int i = index+1; i < fileBuf1.size(); i++)
        {
            if (fileBuf1[i].first.find(subWord) != -1)
            {
                found1 = i;
                i = fileBuf1.size();
                f = 1;
            }
        }
    }
    if (index < fileBuf2.size())
    {
        for (int i = index + 1; i < fileBuf2.size(); i++)
        {
            if (fileBuf2[i].first.find(subWord) != -1)
            {
                found2 = i;
                i = fileBuf2.size();
                f = 1;
            }
        }
    }
    if (f == 0)
    {
        
    }
    else
    {
        index = min(found1,found2);
    }
    redraw();
    

    
}

int NewGit::conflictJump(int index, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2)
{
    int fin = max(fileBuf1.size(), fileBuf2.size());
    if (fileBuf1.size() >= fileBuf2.size())
    {
        for (int i = index + 1; i < fin; i++)
        {
            if (fileBuf1[i].second == CONFLICT)
            {
                return i;
            }
        }
    }
    else
    {
        for (int i = index + 1; i < fin; i++)
        {
            if (fileBuf2[i].second == CONFLICT)
            {
                return i;
            }
        }
    }
    
    return index;
}

void NewGit::saveToFile(const char* fileName, vector<pair<string,int>> &outputBuff)
{
    ofstream file;
    file.open(fileName);
    for (int i = 0; i < outputBuff.size(); i++)
    {
        file << outputBuff[i].first << endl;
    }
    file.close();
}

void NewGit::moveUp()
{
    if (index > 0)
    {
        index--;

    }
    redraw();
}

void NewGit::moveDown()
{
    if (index < max(buff1.size(),buff2.size())-1)
    {
        index++;

    }
    redraw();
}
