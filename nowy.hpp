#ifndef NOWY_HPP_INCLUDED_
#define NOWY_HPP_INCLUDED_
#include "shared.hpp"
#include <string.h>
#include <string>
#include <vector>

using namespace std;

class NewGit : public Tool
{
    public:
        void setEntry(string field, string value);
        string getEntry(string field);
        void init();
        void redraw();
        Backend *backend;
        NewGit();
        void moveUp();
        void moveDown();
        void edit(const char* choice, int index, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2,vector<pair<string,int>> &outputBuff);
        void printBuf(int offX, int startIndex, int maxLines, vector<pair<string,int>> &fileBuf);
        void compare(vector<pair<string,int>> &fileBuf1, vector<pair<string, int>> &fileBuf2);
        int loadFile(vector<pair<string,int>> &buf, const char* fileName);
        int findSubword(const char* subWord, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2);
        int conflictJump(int index, vector<pair<string,int>> &fileBuf1, vector<pair<string,int>> &fileBuf2);
        void saveToFile(const char* fileName, vector<pair<string,int>> &outputBuff);
        void chooseLeft();
        void chooseRight();



    private:
        vector<pair<string,int>> buff1;
        vector<pair<string,int>> buff2;
        vector<pair<string,int>> outputBuff;
        string isSaved, newText,file1,file2, subword,saveFile;
        int index;
        int mode;
        int f1Load, f2Load, height, width;
        
};




#endif